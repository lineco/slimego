/**
 * Created by lijizhuang on 2017/7/6.
 */
export const APP = {
    TAB: 'APP.TAB',
    'NAVIGATION': 'APP.NAVIGATION'
};

export const HOME = {
    SEARCH: 'HOME.SEARCH',
    SUGGEST: 'HOME.SUGGEST'
};

export const CATEGORY = {
    SYNCKEYWORD:'CATEGORY.SYNCKEYWORD',
    SWITCHTOPTAB:'CATEGORY.SWITCHTOPTAB'
};

export const HOTS={
    SYNCHOTS:'HOTS.SYNCHOTS'
};