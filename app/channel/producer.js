/**
 * Created by lijizhuang on 2017/7/27.
 */
const producer = {
    /**
     */
    searchGeneral: (res) => {
        let result = {
            items: []
        };
        let item;

        var total = res['searchResult']['recordCount'];
        var list = res['searchResult']['itemList'];
        if (list) {
            list.forEach((item, index) => {
                newItem = {
                    id: item.id,
                    uk: item.uk,
                    shareId: item.shareId,
                    name: item.name,
                    suffix: item.suffix,
                    ftype: item.ftype,
                    uploadTime: item.uploadTimeChinese,
                    fsId: item.fsId,
                    fileSize: item.fileSizeStr,
                    isFolder: item.isFolder,
                    total: total
                };

                result.items.push(newItem);
            });
        }

        return result.items;
    },

    hotsGeneral: (res) => {
        let result = {
            items: []
        };

        let obj = res["data"];
        let newItem;
        Object.keys(obj).forEach(function (key) {
            newItem = {
                title: key,
                values: obj[key]
            };

            result.items.push(newItem);
        });

        return result.items;
    }
};

export default producer;