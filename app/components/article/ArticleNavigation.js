/**
 * Created by lijizhuang on 2017/7/24.
 */
'use strict';

// React
import React from 'react';
import {Image} from 'react-native';

// Navigation
import { addNavigationHelpers } from 'react-navigation';
import { NavigatorArticle } from './navigationConfiguration';

// Redux
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return {
        navigationState: state.tabArticle
    }
};

class ArticleNavigation extends React.Component {
    static navigationOptions = {
        tabBarLabel: '文章',
        tabBarIcon: ({tintColor}) => (<Image source={require('../../images/icon_tabbar_articlepage.png')} style={[{tintColor: tintColor},{width:25},{height:25}]}/>)
    };

    render(){
        const { navigationState, dispatch, screenProps } = this.props;
        return (
            <NavigatorArticle
                navigation={
                  addNavigationHelpers({
                    dispatch: dispatch,
                    state: navigationState
                  })
                }
                screenProps={screenProps}
            />
        )
    }
}
export default connect(mapStateToProps)(ArticleNavigation)