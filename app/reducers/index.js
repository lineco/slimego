/**
 * Created by lijizhuang on 2017/7/6.
 */
'use strict';

import { combineReducers } from 'redux';

import { NavigatorSearch } from '../components/home/navigationConfiguration'
import { NavigatorHot } from '../components/hot/navigationConfiguration'
import { NavigatorArticle } from '../components/article/navigationConfiguration'
import { TabBar, tabBarReducer } from '../tabbar/navigationConfiguration'

import application from './application';
import home from './home';
import category from './category';
import hots from './hots';

const reducers = combineReducers({
    application,
    home,
    category,
    hots,
    tabBar: tabBarReducer,
    tabHome: (state,action) => NavigatorSearch.router.getStateForAction(action,state),
    tabHot: (state,action) => NavigatorHot.router.getStateForAction(action,state),
    tabArticle: (state,action) => NavigatorArticle.router.getStateForAction(action,state)
});

export default reducers;