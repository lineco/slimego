/**
 * Created by lijizhuang on 2017/8/5.
 */
'use strict';

import createReducer from '../utils/create-reducer'

import {HOTS} from '../constants'

/* search result */
const initialState = {
    actionType:'',
    items:[]
};

const actionHandler = {
    [HOTS.SYNCHOTS]: (state, action) => {
        return Object.assign({}, state, {
            actionType:action.type,
            items: action.result
        })
    }
};

export default createReducer(initialState, actionHandler);