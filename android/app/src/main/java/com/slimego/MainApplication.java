package com.slimego;

import android.app.Application;
import android.support.annotation.Nullable;

import com.cboy.rn.splashscreen.SplashScreenReactPackage;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.microsoft.codepush.react.CodePush;
import com.oblador.vectoricons.VectorIconsPackage;

import java.util.Arrays;
import java.util.List;

// 1. Import the plugin class.
import com.microsoft.codepush.react.CodePush;


public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    // 2. Override the getJSBundleFile method in order to let
    // the CodePush runtime determine where to get the JS
    // bundle location from on each app start
    @Override
    protected String getJSBundleFile() {
      return CodePush.getJSBundleFile();
  }

    @Override
    protected List<ReactPackage> getPackages() {
      // 3. Instantiate an instance of the CodePush runtime and add it to the list of
      // existing packages, specifying the right deployment key. If you don't already
      // have it, you can run "code-push deployment ls <appName> -k" to retrieve your key.
      return Arrays.<ReactPackage>asList(
              new MainReactPackage()
              ,new VectorIconsPackage()
              ,new SplashScreenReactPackage()
              ,new UmengReactPackage()
              ,new CodePush("T2hEJmpw5_0NSfgpePx3Rn0JuwsZ67d48f5c-7365-45f6-8d93-2166e25d77d4", MainApplication.this, BuildConfig.DEBUG)
      );
    }

    //加载offline bundle
    @Nullable
    @Override
    protected String getBundleAssetName() { return "index.android.bundle"; }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
